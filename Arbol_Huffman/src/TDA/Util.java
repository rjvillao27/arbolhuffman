/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDA;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Josue
 */
public class Util {
    
    public static String leetTexto(String nombreArchivo){    
        try(BufferedReader b = new BufferedReader(new FileReader(nombreArchivo))){
        StringBuilder sb = new StringBuilder();
        String cadena;
           while((cadena=b.readLine())!=null){
               sb.append(cadena);
           }
           return sb.toString();
        }catch(Exception e){
            System.out.println(e);
            return null;
        }      
    }
    
    public static HashMap<String,Integer> calcularFrecuencias(String texto){
        char[] caracteres=texto.toCharArray();
        HashMap<String,Integer> mapa=new HashMap<>(); 
        for(char c:caracteres){
            int contador=0;
            if(!mapa.keySet().contains(Character.toString(c))){
                for(char repeticion:caracteres){
                    if(c==repeticion) contador++;
                    mapa.put(Character.toString(c), contador);
                }
            }
        }  
        return mapa;
    }
    
    public static String binarioHexadecimal(String binario){
        StringBuilder binary=new StringBuilder();
        binary.append(binario);
        int val= binario.length();
        int ceros=8-(val%8);
        if(val%8!=0){           
            for (int i=0; i<ceros%8;i++){
                binary.append("0");
            }
        }
        StringBuilder resultado = new StringBuilder();
            for ( int i = 0 ; i < binary.length() ; i +=4 ) {
                int    numero  = Integer.parseInt( binary.substring( i, i+4) , 2 );
                String reprHex = Integer.toString( numero, 16 );
                resultado.append( reprHex );
            }
        for (int i=0; i<ceros%8;i++){
                resultado.append("-");
        }
        return resultado.toString();
    }
    
    public static String hexadecimalBinario(String hexa){
        StringBuilder sb=new StringBuilder();
        char[] numHexa=hexa.toCharArray();
        for (char c:numHexa){
            if(c=='-'){
                sb.deleteCharAt(sb.length()-1);
            }
            else{
                StringBuilder miniBin=new StringBuilder();
                String letra=String.valueOf(c);
                int num= Integer.parseInt(letra, 16);
                String bin=Integer.toString(num,2);
                miniBin.append("0".repeat(4-bin.length()));
                miniBin.append(bin);
                sb.append(miniBin);
            }           
        }
        return sb.toString();
    }
    
    public static void guardarTexto(String nombreArchivo, String texto, HashMap<String,String> mapa) throws IOException{
        try(BufferedWriter bfwriter = new BufferedWriter(new FileWriter(nombreArchivo))){
            bfwriter.write(texto);
            System.out.println("Archivo guardado sin codificador");
        }catch(Exception ex){
            System.out.println(ex);
        }
        escribirCodificador(nombreArchivo.substring(0, nombreArchivo.length()-4),mapa);      
    }
     public static void guardarTexto(String nombreArchivo, String texto){
        try(BufferedWriter bfwriter = new BufferedWriter(new FileWriter(nombreArchivo))){
            bfwriter.write(texto);
            System.out.println("Archivo de texto trasnformado y guardado");
        }catch(Exception ex){
            System.out.println(ex);
        }    
    }
    
    
    private static void escribirCodificador(String nombreArchivo, HashMap<String,String> mapa){
       try(BufferedWriter bfwriter = new BufferedWriter(new FileWriter(nombreArchivo+"_compress.txt"))){
            for(String clave:mapa.keySet()){
                String valor=mapa.get(clave);
                bfwriter.write(clave+"|"+valor+"\n");
            } 
            System.out.println("Archivo codificador guardado exitosamente"); 
        }catch(Exception ex){
            System.out.println(ex);
        } 
    }
    
    public static HashMap<String,String> leerMapa(String nombreArchivo){
       try (BufferedReader b = new BufferedReader(new FileReader(nombreArchivo))){
        String cadena;
        HashMap<String,String> mapa=new HashMap<>();
           while((cadena=b.readLine())!=null){
               String[] linea=cadena.split("\\|");
               String clave = linea[0];
               String valor = linea[1];
               mapa.put(clave, valor);
           }
           return mapa;
        }catch(Exception e){
            System.out.println(e);
            return null;
        }  
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDA;

import static TDA.Util.hexadecimalBinario;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;

/**
 *
 * @author Josue
 */
public class ArbolHuffman{
    private Node root;
    
    private class Node{ 
        private String data;
        private Integer frecuencia;
        private Node left;
        private Node right;
        
        public Node(String data, Integer frecuencia){
            this.data=data;
            this.frecuencia=frecuencia;
        }
    }      
    
    public boolean calcularArbol(HashMap<String,Integer> mapa){
        if(mapa.keySet().size()==0) return false;
        PriorityQueue<Node> cola=new PriorityQueue<>((Node n1, Node n2)->n1.frecuencia-n2.frecuencia);
        for(Entry<String, Integer> e: mapa.entrySet()){
            cola.offer(new Node(e.getKey(),e.getValue()));
        }
        while (cola.size() > 1) {
            Node n1 = cola.poll();
            Node n2 = cola.poll();
            Node parent = new Node(null, n1.frecuencia + n2.frecuencia);
            parent.left = n1;
            parent.right = n2;
            cola.offer(parent);
        }
        root = cola.poll();
        return true;
    }
    
    public HashMap<String, String> calcularCodigos(){
        HashMap<String, String> mapa=new HashMap<>();
        calcularCodigos(null,mapa,root);
        return mapa;
    }
    
    private void calcularCodigos(String codigo, HashMap<String,String> mapa, Node n){
        if(n.left==null && n.right==null){           
            mapa.put(n.data, codigo.substring(4));
        }
        else{
            calcularCodigos(codigo+"0",mapa,n.left);
            calcularCodigos(codigo+"1",mapa,n.right);
        }
    }
    
    public static String codificar(String texto, HashMap<String,String> mapa){
        StringBuilder sb=new StringBuilder();
        char [] listaTexto=texto.toCharArray();
        for(char letra:listaTexto){
            String codigo=mapa.get(Character.toString(letra));
            sb.append(codigo);
        }
        return sb.toString();
    }
    
    
    
    public static String decodificar(String texto, HashMap<String,String> mapa){
        String binary= hexadecimalBinario(texto);
        char[] cadBin = binary.toCharArray();
        StringBuilder sb= new StringBuilder();
        StringBuilder temp= new StringBuilder();
        for (int i = 0; i < cadBin.length; i++) {
            temp.append(cadBin[i]);
            String val = obtenerClave(mapa,temp.toString());
            if (val == null) {
                continue;
            }                   
            sb.append(val);
            temp.setLength(0); 
        }
        return sb.toString();
    }
    
    private static String obtenerClave(HashMap<String,String> mapa, String valor){
        for (Entry<String, String> e : mapa.entrySet()) {
            String key = e.getKey();
            String value = e.getValue();
            if (valor.equals(value)) { 
                return key;
            }
        }
        return null;
    }
    
    public void preOrden(){
        preOrden(root);
    }
    private void preOrden(Node n){
         if(n!=null){
             System.out.println(n.data);
             preOrden(n.left);
             preOrden(n.right);
         }
    }

}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbolhuffman;



import GUI.Ventana;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Josue
 */
public class ArbolHuffman extends Application{

    public static void main(String[] args) {
        launch();
    }
    
    @Override
     public void start(Stage primaryStage) {
        Scene s= new Scene(new Ventana().getRoot(),1000,600);
        primaryStage.setResizable(false);
        primaryStage.setTitle("Arbol Huffman");
        primaryStage.setScene(s);
        primaryStage.show();
    } 
}

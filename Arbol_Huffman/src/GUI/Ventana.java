/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import TDA.ArbolHuffman;
import TDA.Util;
import java.io.File;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import arbolhuffman.Arbol_Huffman.*;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Josue
 */
public class Ventana {
    private BorderPane root = new BorderPane();
    private Button btnComprimir=new Button();
    private Button btnDescomprimir=new Button();
    private VBox contenedorBotones=new VBox();
    private Label lblMenu= new Label("Opciones del programa:");
    public BorderPane getRoot(){
        return root;
    }
    
    public Ventana(){
        organizarBotones();
    }
    
    private void organizarBotones(){
        asignarNombres();
        contenedorBotones.getChildren().addAll(lblMenu,btnComprimir,btnDescomprimir);
        contenedorBotones.setSpacing(15);
        contenedorBotones.setAlignment(Pos.CENTER);
        root.setCenter(contenedorBotones);
        btnComprimir.setOnAction(e->comprimir());
        btnDescomprimir.setOnAction(e2->descomprimir());
    }
    
    private void asignarNombres(){
        btnComprimir.setText("Comprimir");     
        btnDescomprimir.setText("Descomprimir");        
    }
    
    private static void comprimir(){
        FileChooser filechooser=new FileChooser();
        filechooser.setTitle("Buscar archivo");
        filechooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Archivos de texto","*.txt"),
                                                  new FileChooser.ExtensionFilter("Todos los archivos","*.*"));   
        File archivo=filechooser.showOpenDialog(null);
        comprimirArchivo(archivo.getPath());
        
    }
    
    private static void comprimirArchivo(String ruta){
         String  texto=Util.leetTexto(ruta);        
         HashMap<String, Integer> mapa=Util.calcularFrecuencias(texto);  
         ArbolHuffman arbol=new ArbolHuffman();
         arbol.calcularArbol(mapa);              
         HashMap<String,String> codigosArbol=arbol.calcularCodigos();
         String textoBin=arbol.codificar(texto,codigosArbol);
         String textoHexa=Util.binarioHexadecimal(textoBin);
         FileChooser filechooser=new FileChooser();
         filechooser.setTitle("Guardar Archivo");
         filechooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Archivos de texto","*.txt"),
                                                  new FileChooser.ExtensionFilter("Todos los archivos","*.*"));                                            
         File archivo=filechooser.showSaveDialog(null);
         System.out.println(archivo.getPath());
         Util.guardarTexto(archivo.getPath(), textoHexa, codigosArbol);
    }
    
   
    
    private static void descomprimir(){
        FileChooser filechooser=new FileChooser();
        filechooser.setTitle("Buscar archivo");
        filechooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Archivos de texto","*.txt"),
                                                  new FileChooser.ExtensionFilter("Todos los archivos","*.*"));   
        File archivo=filechooser.showOpenDialog(null);
        descomprimirArchivo(archivo.getPath(),archivo.getPath().substring(0, archivo.getPath().length()-4)+"_compress.txt");
    }
    
    private static void descomprimirArchivo(String ruta, String rutaCodigos){
         HashMap<String,String> codigosArbol=Util.leerMapa(rutaCodigos);
         String texto=Util.leetTexto(ruta);
         String textoOriginal=ArbolHuffman.decodificar(texto, codigosArbol);
         FileChooser filechooser=new FileChooser();
        filechooser.setTitle("Buscar archivo");
        filechooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Archivos de texto","*.txt"),
                                                  new FileChooser.ExtensionFilter("Todos los archivos","*.*"));   
        File archivo=filechooser.showSaveDialog(null);
         
         Util.guardarTexto(archivo.getPath(), textoOriginal);
    }
   
}
